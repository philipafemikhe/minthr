package com.minthr.MintHR.controller;

import com.minthr.MintHR.entity.Card;
import com.minthr.MintHR.entity.CardValidity;
import com.minthr.MintHR.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class CardController {
    @Autowired
    CardService cardService;
    @RequestMapping("card/get")
    public Card getCard(){
        Card card = this.cardService.getCard();
        return card;
    }

    @RequestMapping("/card-scheme/verify/{id}")
    public CardValidity verifyCard(@PathVariable int id){
        return cardService.verifyCard(id);
    }

    @RequestMapping("/card-scheme/stats?start=1&limit=3")
    public String cardStatistics(@RequestParam int start, int limit){
        return cardService.cardStatistics(start, limit);
    }

}
