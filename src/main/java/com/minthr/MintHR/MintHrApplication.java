package com.minthr.MintHR;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MintHrApplication {

	public static void main(String[] args) {
		SpringApplication.run(MintHrApplication.class, args);
	}

}
