package com.minthr.MintHR.repository;

import com.minthr.MintHR.entity.Card;
import org.springframework.data.repository.CrudRepository;

public interface CardRepository extends CrudRepository<Card,Integer> {
}
