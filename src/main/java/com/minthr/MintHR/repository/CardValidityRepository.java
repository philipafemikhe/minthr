package com.minthr.MintHR.repository;

import com.minthr.MintHR.entity.CardValidity;
import org.springframework.data.repository.CrudRepository;

public interface CardValidityRepository extends CrudRepository<CardValidity, Integer> {
}
