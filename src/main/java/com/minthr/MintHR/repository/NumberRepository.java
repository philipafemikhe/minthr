package com.minthr.MintHR.repository;

import com.minthr.MintHR.entity.Number;
import org.springframework.data.repository.CrudRepository;

public interface NumberRepository extends CrudRepository<Number, Integer> {
}
