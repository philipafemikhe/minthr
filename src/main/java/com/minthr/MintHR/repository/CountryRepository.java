package com.minthr.MintHR.repository;

import com.minthr.MintHR.entity.Country;
import org.springframework.data.repository.CrudRepository;

public interface CountryRepository extends CrudRepository<Country, Integer> {
}
