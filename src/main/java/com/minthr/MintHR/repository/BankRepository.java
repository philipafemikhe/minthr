package com.minthr.MintHR.repository;

import com.minthr.MintHR.entity.Bank;
import org.springframework.data.repository.CrudRepository;

public interface BankRepository extends CrudRepository<Bank, Integer> {
}
